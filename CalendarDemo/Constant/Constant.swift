//
//  Constant.swift
//  CalendarDemo
//
//  Created by iMac on 30/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation

struct Constant {
    static let monthNameForSection = ["Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec","Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec"]
    static let weekArray = ["Sun","Mon","Tue","Wed","Thu","Fri","Sat"]
    static let kWeekCollectionViewCell = "WeekCollectionViewCell"
    static let kCalendarTableViewCell = "CalendarTableViewCell"
    static let kCalendarCollectionViewCell = "CalendarCollectionViewCell"
}
