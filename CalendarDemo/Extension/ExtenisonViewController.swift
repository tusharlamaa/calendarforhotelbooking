//
//  ExtenisonViewController.swift
//  CalendarDemo
//
//  Created by iMac on 30/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation
import UIKit

extension ViewController{
    
    func setUI(){
        weekCollectionView.delegate = self
        weekCollectionView.dataSource = self
        weekCollectionView.reloadData()
        
        calendarTableView.delegate = self
        calendarTableView.dataSource = self
        calendarTableView.reloadData()
    }
    
    func getDaysForNextMonth(weekDay: String) -> Int{
        switch weekDay {
        case "Sun":
            return 0
        case "Mon":
            return 1
        case "Tue":
            return 2
        case "Wed":
            return 3
        case "Thu":
            return 4
        case "Fri":
            return 5
        case "Sat":
            return 6
        default:
            break
        }
        return 0
    }
    
    
    //Getting the week days to check in which week day does 1st of every month lies
    func getFirstDate(index: Int) -> Int{
        var exactDate = ""
        if index == 0{
            let formatter = DateFormatter()
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            let myString = formatter.string(from: Date())
            let yourDate = formatter.date(from: myString)
            formatter.dateFormat = "yyyy-MM-dd HH:mm"
            let myStringafd = formatter.string(from: yourDate!)
            let newDate = myStringafd.components(separatedBy: "-")
            
            currentMonth = newDate[1]
            currentYear = newDate[0]
            currentDate = "01"
            exactDate = "\(currentYear)-\(currentMonth)-\(currentDate)"
            if currentMonth == "12"{
                isFirstMonthDec = true
            }
        }else{
            currentDate = "01"
            if isFirstMonthDec{
                isFirstMonthDec = false
                currentMonth = "01"
                currentYear = "\((Int(currentYear) ?? 0) + 1)"
                exactDate = "\(currentYear)-\(currentMonth)-\(currentDate)"
            }else{
                if currentMonth == "12"{
                    decemberMonthBool = true
                    exactDate = "\(currentYear)-\(currentMonth)-\(currentDate)"
                }else{
                    if decemberMonthBool {
                        decemberMonthBool = false
                        currentYear = "\((Int(currentYear) ?? 0) + 1)"
                        currentMonth = "01"
                        currentDate = "01"
                        exactDate = "\(currentYear)-\(currentMonth)-\(currentDate)"
                    }else{
                        currentMonth = "\((Int(currentMonth) ?? 0) + 1)"
                        if currentMonth.count == 1{
                            currentMonth = "\(0)" + currentMonth
                        }
                        currentDate = "01"
                        exactDate = "\(currentYear)-\(currentMonth)-\(currentDate)"
                    }
                }
            }
        }
        
        return getDaysForNextMonth(weekDay: Utility.convertStringToDate(date: exactDate).weekdayName)
        
    }
    
    func addMonthNameYear(){
        
        //getting the index of the current month from monthArray
             index = Constant.monthNameForSection.enumerated().filter {
                 $0.element == Date().monthName
             }.map{$0.offset}[0]
             
                     
             for j in 0..<Constant.monthNameForSection.count{
                 if j >= index{
                     if j ==  index{
                         //Adding the month for collection view
                         sectionForCollectionView.append(Constant.monthNameForSection[j])
                         
                         //Adding the month and year to display for user
                         sectionArray.append(Constant.monthNameForSection[j] + " " + "\( Date().OnlyYear)")
                     }else{
                         //Adding the month for collection view
                         sectionForCollectionView.append(Constant.monthNameForSection[j])
                     
                         //Checking if month is december or not if found december adding a year to display
                         if Constant.monthNameForSection[j] == "Dec"{
                             addYear = true
                             sectionArray.append(Constant.monthNameForSection[j] + " " +  "\(Date().OnlyYear)")
                         }else{
                             if addYear{
                                 sectionArray.append(Constant.monthNameForSection[j] + " " +  "\( (Int(Date().OnlyYear) ?? 0) + 1)")
                             }else{
                                 sectionArray.append(Constant.monthNameForSection[j] + " " +  "\(Date().OnlyYear)")
                             }
                         }
                     }
                     
                     if j > index{
                         if Constant.monthNameForSection[j] == Date().monthName{
                             break
                         }
                     }
                 }
             }
    }
    
    
    func addDaysForMonth(){
        //Getting days array and also checking if the year is a leap year or not
               for k in 0..<sectionArray.count{
                   let year = sectionArray[k].components(separatedBy: " ")
                   if year[0] == "Jan" || year[0] == "Mar" || year[0] == "May" || year[0] == "Jul" || year[0] == "Aug" || year[0] == "Oct" || year[0] == "Dec"{
                       daysArray.append(32)
                   }else if year[0] == "Feb" {
                       if (Int(year[1]) ?? 0) % 4 == 0{
                           if year[0] == "Feb"{
                               daysArray.append(30)
                           }
                       }else{
                           if year[0] == "Feb"{
                               daysArray.append(29)
                           }
                       }
                   }else{
                       daysArray.append(31)
                   }
               }
    }
    
    func addDaysInModel(){
        
        //Adding days in model Class as per month and also adding zero in the array to hide cell in collection view
        var weekDays = 0
        var increaseCountOfWeek = false
        var isComingFromWeekday = false
        
        for days in 0..<daysArray.count{
            var value = [Int]()
           
            if increaseCountOfWeek {
                increaseCountOfWeek = false
                isComingFromWeekday = true
                weekDays = weekDays + 1
            }
            
            for k in 1..<daysArray[days]{
            
                if days == 0{
                    
                    for _ in 0..<getFirstDate(index: days){
                        value.append(0)
                    }
                    
                    weekDays = getDaysForNextMonth(weekDay: Date().weekdayName) + 1
                    isComingFromWeekday = true
                    let data = Date().string(format: "yyyy-MM-dd").components(separatedBy: "-")
                    if Int(data[2]) ?? 0 <= k{
                        if Int(data[2]) ?? 0 == k{
                            
                            if getDaysForNextMonth(weekDay: Date().weekdayName)>0{
                               
                                value.removeAll()
                                for _ in 0..<getDaysForNextMonth(weekDay: Date().weekdayName){
                                    value.append(0)
                                }
                            }
                        }
                       value.append(k)
                    }
                } else{
                    
                    if isComingFromWeekday{
                       isComingFromWeekday = false
                        increaseCountOfWeek = true
                        if weekDays > 0{
                            for _ in 0..<getFirstDate(index: days){
                                value.append(0)
                            }
                        }
                    }
                    
                   value.append(k)
                }
            }
           
            dayModel.append(DaysModel(daysArray: value, weekDay: 0))
            value.removeAll()
        }
    }
   
}

