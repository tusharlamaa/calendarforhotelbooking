//
//  DayModel.swift
//  CalendarDemo
//
//  Created by iMac on 30/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation


class DaysModel {
    var daysArray = [Int]()
    var weekDay = 0
    
    public init(daysArray: [Int],weekDay: Int){
        self.daysArray = daysArray
        self.weekDay = weekDay
    }
}
