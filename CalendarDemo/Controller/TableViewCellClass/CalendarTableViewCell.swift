//
//  CalendarTableViewCell.swift
//  CalendarDemo
//
//  Created by iMac on 16/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit

protocol CalendarDelegate {
    func reloadCollectionView()
}

class CalendarTableViewCell: UITableViewCell {
    @IBOutlet weak var calendarDateCollectionView: UICollectionView!
    var sectionArray = [String]()
    var monthsDays = [String]()
    var daysArray = [Int]()
    var days = [String]()
    var dayModel = [DaysModel]()
    var indexForSection = 0
    var sectionDictionary = NSMutableDictionary()
    var dateDictionary = NSMutableDictionary()
    var delegate: CalendarDelegate?
    var handleUserClickCount = 0
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func reloadCollection(index: Int){
        indexForSection = index
        calendarDateCollectionView.delegate = self
        calendarDateCollectionView.dataSource = self
        calendarDateCollectionView.reloadData()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}


extension CalendarTableViewCell: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return dayModel[indexForSection].daysArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.kCalendarCollectionViewCell, for: indexPath) as? CalendarCollectionViewCell else{
            return UICollectionViewCell()
        }
    
        if dayModel[indexForSection].daysArray[indexPath.row] == 0{
            cell.dateCellLabel.text = ""
        }else{
            if sectionDictionary.count>0{
               
                if indexForSection == sectionDictionary.value(forKey: "\(indexForSection)") as! Int{
                   
                    if handleUserClickCount == 1{
                        if dayModel[indexForSection].daysArray[indexPath.row] == dateDictionary.value(forKey: "\(indexForSection)") as! Int{
                          cell.dateCellLabel.backgroundColor = .systemBlue
                        }
                    }else{
                        if dayModel[indexForSection].daysArray[indexPath.row] <= dateDictionary.value(forKey: "\(indexForSection)") as! Int{
                            cell.dateCellLabel.backgroundColor = .systemBlue
                        }else{
                            cell.dateCellLabel.backgroundColor = .clear
                        }
                    }
                }
            }
            else{
                cell.dateCellLabel.backgroundColor = .clear
            }
            
            cell.dateCellLabel.textAlignment = .center
            cell.dateCellLabel.text = "\(dayModel[indexForSection].daysArray[indexPath.row])"
        }
        
        
        return cell
    }
    
    
     func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize
       {
        return CGSize(width: self.frame.size.width/8, height: 35)
       }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
         handleUserClickCount = handleUserClickCount + 1
        
        if handleUserClickCount == 3 {
            handleUserClickCount = 0
            handleUserClickCount = handleUserClickCount + 1
            sectionDictionary.removeAllObjects()
            dateDictionary.removeAllObjects()
           
//            dateDictionary.setValue(dayModel[indexForSection].daysArray[indexPath.row], forKey: "\(indexForSection)")
//            sectionDictionary.setValue(indexForSection, forKey: "\(indexForSection)")
        }else{
           
            dateDictionary.setValue(dayModel[indexForSection].daysArray[indexPath.row], forKey: "\(indexForSection)")
            sectionDictionary.setValue(indexForSection, forKey: "\(indexForSection)")
        }
        
        print(handleUserClickCount)
        delegate?.reloadCollectionView()
        
    }
    
}
