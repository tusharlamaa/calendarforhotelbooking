//
//  ViewController.swift
//  CalendarDemo
//
//  Created by iMac on 16/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import UIKit


class ViewController: UIViewController {
    @IBOutlet weak var weekCollectionView: UICollectionView!
    @IBOutlet weak var calendarTableView: UITableView!
    
    var sectionArray = [String]()
    var sectionForCollectionView = [String]()
    var daysArray = [Int]()
    var index = 0
    var dayModel = [DaysModel]()
    var currentYear = ""
    var currentMonth = ""
    var currentDate = ""
    var decemberMonthBool = false
    var isFirstMonthDec = false
    var addYear = false
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setUI()
        addMonthNameYear()
        addDaysForMonth()
        addDaysInModel()
    }
  
}

extension ViewController: UICollectionViewDelegate,UICollectionViewDataSource,UICollectionViewDelegateFlowLayout{
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return Constant.weekArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constant.kWeekCollectionViewCell, for: indexPath) as? WeekCollectionViewCell else{
            return UICollectionViewCell()
        }
        cell.weekCell.text = Constant.weekArray[indexPath.row]
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: self.view.frame.size.width/7, height: 35.0)
    }
    
}

extension ViewController: UITableViewDelegate,UITableViewDataSource{

    func numberOfSections(in tableView: UITableView) -> Int {
        sectionArray.count
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return sectionArray[section]
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: Constant.kCalendarTableViewCell) as? CalendarTableViewCell else{
            return UITableViewCell()
        }
        cell.calendarDateCollectionView.isScrollEnabled = false
        cell.sectionArray = sectionForCollectionView
        cell.daysArray = daysArray
        cell.dayModel = dayModel
        cell.delegate = self
        cell.reloadCollection(index: indexPath.section)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if indexPath.section == 0{
            let daysLeft = Calendar.current.dateComponents([.day], from: Date(), to: Date().endOfMonth()!).day ?? 0
            if daysLeft <= 7{
                return 50.0
            }else if daysLeft <= 14{
                return 100.0
            }else if  daysLeft <= 28{
                return 150.0
            }else{
                return 200.0
            }
        }else{
            return 200.0
        }
    }
    
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int){
        view.tintColor = UIColor.systemTeal
        let header = view as! UITableViewHeaderFooterView
        header.textLabel?.textColor = UIColor.white
        header.textLabel?.textAlignment = .center
         header.textLabel?.font = UIFont.systemFont(ofSize: 13.0)
    }
}



extension ViewController: CalendarDelegate{
    func reloadCollectionView(){
           calendarTableView.reloadData()
       }
}
