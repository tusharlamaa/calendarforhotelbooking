//
//  Utility.swift
//  CalendarDemo
//
//  Created by iMac on 30/12/2019.
//  Copyright © 2019 iMac. All rights reserved.
//

import Foundation


class Utility: NSObject{
    
    
    static func convertStringToDate(date: String) -> Date {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd"
        let newDate = dateformatter.date(from: date)
        dateformatter.dateFormat = "dd-MM-yyyy"
        // dateformatter.timeZone = TimeZone(identifier: "UTC")
        if let newDate = newDate {
            return newDate as Date
        } else {
            return Date()
        }
    }
    
}
